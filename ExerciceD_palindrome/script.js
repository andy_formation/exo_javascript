function isPalindrome(wrd) {
	
	let wrdInverse = wrd.split("");
	wrdInverse = wrdInverse.reverse();
	wrdInverse = wrdInverse.join("");
	
	if (wrd === wrdInverse)
		{
			console.log(true)
		} else {
			console.log(false)
		}
}

isPalindrome("rotor");
isPalindrome("abacus");
isPalindrome("radar");
isPalindrome("scholars");
isPalindrome("madam");