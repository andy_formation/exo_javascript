/*function minMax(arr) {
	var min = 0;
	var max = 0;
	
	for (let i = 0; i < arr.length; i++) {
		max = i == 1;
		min = i == 1;
		
		if(max < i == i+1) {
			max = i+1;
		}
		
		if(min > i == i+1) {
			min = i+1;
		}
	}
	return(max, min);
}*/

// SOLUTION *********************************************************

function minMax(arr) {
    var returnArr = [];
    var min = arr[0];
    var max = arr[0];
    
    for(var i = 1; i < arr.length; i++) {
      if(arr[i] < min) {
        min = arr[i];
      }
      if(arr[i] > max) {
        max = arr[i];
      }
    }
    
    returnArr.push(min);
    returnArr.push(max);
    
    return returnArr;
  }