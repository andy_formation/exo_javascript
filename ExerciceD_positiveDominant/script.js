function isPositiveDominant(a) {
	let positif = 0;
	let negatif = 0;
	
	for(i = 0; i < a.length; i++) {
		if(a[i] > 0 && a[i] != a[i + 1])
			{
				positif = positif + 1;
			} 
		else if (a[i] < 0)
			{
				negatif = negatif + 1
			}
		
	}
	if (positif > negatif) {
		console.log(true)
	} else {
		console.log(false)
	}
	
}

isPositiveDominant([1, 1, 1, 1, -3, -4])
isPositiveDominant([5, 99, 832, -3, -4])
isPositiveDominant([5, 0])
isPositiveDominant([0, -4, -1])
isPositiveDominant([1, 2, 3, -1])
isPositiveDominant([1, 0, 0, -1])