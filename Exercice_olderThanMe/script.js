class Person {
	constructor(name, age) {
		this.name = name;
		this.age = age;

	}

	compareAge(other) {
		var me = this;
		if (other.age > me.age)
			{
				console.log(other.name + " is older than me");
			}
	 	else if (other.age == me.age) 
		{
			console.log (other.name + " is the same age than me");
		}
		else {
			console.log(other.name + " is younger than me");
		};
    }
}
var andy = new Person("Andy", 21);
console.log(andy);
var samuel = new Person("Samuel", 34);
console.log(samuel);
samuel.compareAge(andy);