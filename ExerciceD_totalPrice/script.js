function getTotalPrice(groceries) {

    let result = 0;

    for (i of groceries) {
			result = result + i.price;
		}
	console.log(result)
}

getTotalPrice([
	{ product: "Milk", quantity: 1, price: 1.50 }
])

getTotalPrice([
	{ product: "Milk", quantity: 1, price: 1.50 },
	{ product: "Cereals", quantity: 1, price: 2.50 }
])


getTotalPrice([
	{ product: "Milk", quantity: 1, price: 1.50 },
	{ product: "Eggs", quantity: 12, price: 0.10 },
	{ product: "Bread", quantity: 2, price: 1.60 },
	{ product: "Cheese", quantity: 1, price: 4.50 }
])




/***************************************
 * for (i in groceries) {
 * 	result = result + i.price (or groceries.price)
 * }
 */