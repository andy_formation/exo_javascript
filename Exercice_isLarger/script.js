function whichIsLarger(f, g) {
	if (f>g)
		{
			f =  f.toString();
			console.log(f);
		}
	else if (f<g) 
		{
			g =  g.toString();
			console.log(g);
		}
	else {
		console.log("neither");
	}
}

f = 7 
g = 6
whichIsLarger(f, g)

/* our function will be passed two functions, f and g, that don't take any parameters. Your function has to call them, and return a string which indicates which function returned the larger number.

    If f returns the larger number, return the string f.
    If g returns the larger number, return the string g.
    If the functions return the same number, return the string neither.*/