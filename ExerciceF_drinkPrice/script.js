function sortDrinkByPrice(drinks) {
	let result = [];

    result = drinks.sort((a, b) => (a.price > b.price) ? 1 : -1 );

    console.log(result);
    
}

sortDrinkByPrice(
[
	{name: 'water', price: 120}, 
	{name: 'lime', price: 80}, 
	{name: 'peach', price: 90}
]);

