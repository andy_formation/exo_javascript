function convert(deg) {
	let result = 0;
	if (deg.indexOf("F") != -1) {
		let index = deg.indexOf("F");
		deg.substr(index)
		deg = parseInt(deg, 10);
		result = (deg-32) * 5/9
	  result = Math.round(result);
		console.log(result + "°C");
	} else if (deg.indexOf("C") != -1) {
		let index = deg.indexOf("C");
		deg.substr(index)
		deg = parseInt(deg, 10);
		result = deg * 9/5+32
		result = Math.round(result);
		console.log(result + "°F");
	} else {
		console.log("Error");
	}
	
}

convert('35°C') // 95°F 
convert('18°C') // 64°F
convert('0°F') // -18°C
convert('100°C') // 212°F
convert('69°F') // 21°C
convert('159°C') // 318°F
convert('-40°C') // -40°F
convert('-40°F') // -40°C
convert('16°C') // 61°F
convert('500°C') // 932°F
convert('33') // Error
convert('19°F') // -7°C
convert('85°C') // 185°F
convert('0°C') // 32°F
convert('1777°F') // 969°C
convert('-90°C') // -130°F
convert('16°F') // -9°C
convert('180°C') // 356°F
convert('7K') // Error
convert('100°F') // 38°C