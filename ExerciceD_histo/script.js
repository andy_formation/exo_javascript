function histogram(arr, char) {

  let result;
  let multichar = "";

    for (i = 0; i < arr.length; i++) {
      result =  arr[i];
      multichar = multichar + char.repeat(result);
      multichar = multichar + "\n";
    }
    console.log(multichar);

}

histogram([2,4,5,6], "o")

histogram([4,2], "*")

histogram([20,1,12], "H")
